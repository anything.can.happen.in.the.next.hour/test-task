/* eslint-disable react/state-in-constructor */
/* eslint-disable react/prop-types */
import React, { Component } from 'react'

import ST from './index.scss'

class Input extends Component {
  state = {
    value: '',
  }


  onChange = (ev) => {
    const inputValue = ev.target.value
    this.setState({ value: inputValue })
  }

  onBlur = (ev) => {
    this.props.onChange(this.state.value)
  }

  render() {
    const {
      value, placeholder, max, min, name
    } = this.props

    return (
      <input
        value={value}
        type={name}
        className={ST.default}
        placeholder={placeholder}
        onChange={this.onChange}
        name={name}
        maxLength={max}
        minLength={min}
        onBlur={this.onBlur}
      />
    )
  }
}

export default Input
