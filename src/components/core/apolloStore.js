import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-client-preset'
import { InMemoryCache } from 'apollo-client-preset'
import React from 'react'

const token ='a8e44946643b1bbd77c55f009a9770c9b51d153e'

const httpLink = {
  uri:'https://api.github.com/graphql',
  headers: {
    authorization: `Bearer ${token}`
  }
}

const client = new ApolloClient({
  link: new HttpLink(httpLink),
  cache: new InMemoryCache()
})

export default function(props){
    return  <ApolloProvider client={client}>
                { props.children }
            </ApolloProvider>
}