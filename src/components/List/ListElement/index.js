import React, {Component} from 'react'
import ST from './index.scss'
import star from '../../image/star.png'



class ListElement extends Component {
  render() {
    const { link, name } = this.props
    return(
      <div className={ST.container}>
        <img src={star} alt="Some img here"/>
        <div className={ST.data}>
          <p id={ST.name}>{name}</p>
          <p id={ST.link}>{link}</p>
        </div>

      </div>
    )
  }
}


export default ListElement