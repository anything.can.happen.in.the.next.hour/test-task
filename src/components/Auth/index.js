import React from 'react'
import auth from '../core/authStore'
import ST from './index.scss'
import Input from '../Input'


class Auth extends React.Component {

  state = {
    login:'',
    password:''
  }

  onClickHandler = () =>{
    const {login, password} = this.state
    auth.tryAuthorize(login, password)
  }
  setLogin = (login) => {
    this.setState({login})
  }

  setPassword = (password) =>{
    this.setState({password})
  }

  render(){
    return(
      <div className={ST.wrapper}>
      <div className={ST.container}>
        <div className={ST.login}>
            Login: 
            <Input name='login' onChange={this.setLogin} placeholder='Login' />
        </div>

        <div className={ST.password}>
           Password: 
            <Input name='password' onChange={this.setPassword} placeholder='Password' />
        </div>

        <div 
          className={ST.button}
          onClick={this.onClickHandler}
        >
          Войти
        </div>
      </div>
      </div>
    )
  }
}


export default Auth