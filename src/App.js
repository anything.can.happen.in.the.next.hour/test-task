import React, { Component } from 'react'
import auth from './components/core/authStore'
import Auth from './components/Auth'
import MainList from './components/List/MainList'
import { observer } from 'mobx-react'


@observer
class App extends Component {
  render() {
    return  auth.isAuthorized ? <MainList /> : <Auth />
  }
}
export default App 
