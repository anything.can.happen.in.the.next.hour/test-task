import * as React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import Apollo from './components/core/apolloStore'
import './index.scss'
import { HashRouter } from "react-router-dom"
import {createBrowserHistory} from 'history'


const history = createBrowserHistory() 


const container = document.createElement('div')
container.id = 'root'
document.body.appendChild(container)

ReactDOM.render(
  <Apollo >
    <HashRouter history={history}>
      <App />
   </HashRouter>
  </Apollo>, 
  container
)
